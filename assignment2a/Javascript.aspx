﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Javascript.aspx.cs" Inherits="assignment2a.Javascript" %>
<asp:Content ID="Content1" ContentPlaceHolderID="idea_content" runat="server">
     <div class="jumbotron">
    <p>JavaScript is the <b>Programming Language</b> for the Web.</p>
    <p>JavaScript can update and change both <b>HTML</b> and <b>CSS.</b></p>
    <p>JavaScript can <b>calculate</b>, <b>manipulate</b> and <b>validate</b> data.</p></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Code1_content" runat="server">
     <div class="jumbotron">
<asp:image height="300" width="300" runat="server" id="PNGimge1" imageurl="image/js_img2.PNG"/>
</div>
<br/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Code2_content" runat="server">
    <div class="jumbotron">
<asp:image height="300" width="300" runat="server" id="PNGimge2" imageurl="image/js_img3.PNG"/>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Code3_content" runat="server">
    <div class="jumbotron">
   <ul>
    <li class="t"><a href="https://getbootstrap.com/docs/4.1/getting-started/javascript/"/>Getbootstrap</li>
    <li class="t"><a href="https://www.codecademy.com/learn/introduction-to-javascript"/>Codecademy</li>
    <li class="t"><a href="https://developer.mozilla.org/bm/docs/Web/JavaScript"/>Mozilla</li>
   </ul>
  </div>
</asp:Content>
