﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CSS.aspx.cs" Inherits="assignment2a.Digitaldesign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="idea_content" runat="server">
     <div class="jumbotron">
    <p>CSS stands for <b>Cascading Style Sheets.</b></p>
    <p>CSS describes <b>how HTML elements are to be displayed on screen, paper, or in other media.</b></p>
    <p>CSS can control the <b>layout of multiple web pages all at once.</b></p></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Code1_content" runat="server">
     <div class="jumbotron">
<asp:image height="300" width="300" runat="server" id="PNGimge1" imageurl="image/css1.PNG"/>
</div>
<br/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Code2_content" runat="server">
    <div class="jumbotron">
<asp:image height="300" width="300" runat="server" id="PNGimge2" imageurl="image/css2.PNG"/>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Code3_content" runat="server">
    <div class="jumbotron">
   <ul>
    <li class="t"><a href="http://www.htmldog.com/guides/css/beginner/"/>Htmldog</li>
    <li class="t"><a href="https://www.w3schools.com/css/"/>W3schools</li>
    <li class="t"><a href="https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS"/>Mozilla</li>
   </ul>
  </div>
</asp:Content>
