﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SQL.aspx.cs" Inherits="assignment2a.Webprogramming" %>
<asp:Content ID="Content1" ContentPlaceHolderID="idea_content" runat="server">
    <div class="jumbotron">
    <p>SQL stands for <b>Structured Query Language.</b> 
    <p>SQL is a standard language for <b>storing, manipulating and retrieving data</b> in databases. </p> 
    <p>SQL lets you <b>access and manipulate databases.</b></p>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Code1_content" runat="server">
    <div class="jumbotron">
<asp:image height="300" width="300" runat="server" id="PNGimge1" imageurl="image/sql1.PNG"/>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Code2_content" runat="server">
    <div class="jumbotron">
<asp:image height="300" width="300" runat="server" id="PNGimge2" imageurl="image/sql2.PNG"/>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Code3_content" runat="server">
    <div class="jumbotron">
       <ul>
    <li class="t"><a href="https://www.datacamp.com/courses/intro-to-sql-for-data-science"/>Datacamp</li>
    <li class="t"><a href="https://www.codecademy.com/learn/learn-sql"/>Codecademy</li>
    <li class="t"><a href="https://www.w3schools.com/sql"/>W3schools</li>
   </ul>
    </div>
</asp:Content>
